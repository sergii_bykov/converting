package converter;

public class Time {

    public double sec;
    public double min;
    public double hour;
    public double day;
    public double week;
    public double month;
    public double astronomicalYear;

    public double convert(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }


    public double convertSecToMin() {
        double min = sec/60;
        return convert(min, 10);
    }

    public double convertMinToSec() {
        double sec = min*60;
        return convert(sec, 10);
    }

//  ______________________________________________

    public double convertSecToHour() {
        double hour = sec/3600;
        return convert(hour, 10);
    }

    public double convertHourToSec() {
        double sec = hour*3600;
        return convert(sec, 10);
    }

//  ______________________________________________

    public double convertSecToDay() {
        double day = sec/3600/24;
        return convert(day, 10);
    }

    public double convertDayToSec() {
        double sec = day*24*3600;
        return convert(sec, 10);
    }

//  _______________________________________________

    public double convertSecToWeek() {
        double week = sec/3600/24/7;
        return convert(week, 10);
    }

    public double convertWeekToSec() {
        double sec = week*3600*24*7;
        return convert(sec, 10);
    }

//  ________________________________________________

    public double convertSecToMonth() {
        double month = sec/3600/24/30.4167;
        return convert(month, 10);
    }

    public double convertMonthToSec() {
        double sec = month*3600*24*30.4167;
        return convert(sec, 10);
    }

//  _________________________________________________

    public double convertSecToYear() {
        double year = sec/3600/24/365;
        return convert(year, 10);
    }

    public double convertYearToSec() {
        double sec = astronomicalYear *3600*24*365;
        return convert(sec, 10);
    }

}



