package converter;

public class Length {

    public double meter;
    public double kilometers;
    public double mile;
    public double nauticalMiles;
    public double cable;
    public double league;
    public double foot;
    public double yard;


    public double convert(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }


    public double convertMToKm() {
        double kilometers = meter /1000;
        return convert(kilometers, 10);
    }

    public double convertKmToM() {
        meter = kilometers*1000;
        return convert(meter, 10);
    }


    public double convertMToMile() {
        double miles = meter /1609;
        return convert(miles, 10);
    }

    public double convertMileToM() {
        double meters = mile *1609;
        return convert(meters, 10);
    }


    // nautical mile - морская миля
    public double convertMToNauticalMile() {
        double nauticalMiles = meter /1852;
        return convert(nauticalMiles, 10);
    }

    public double convertNauticalMileToM() {
        double meters = nauticalMiles*1852;
        return convert(meters, 10);
    }

    // cable - сантиметр
    public double convertMToCable() {
        double cables = meter /185.2;
        return convert(cables, 10);
    }

    public double convertCableToM() {
        double meters = cable *185.2;
        return convert(meters, 10);
    }

    // league - морская лига
    public double convertMToLeague() {
        double leagues = meter /5556;
        return convert(leagues, 10);
    }

    public double convertLeagueToM() {
        double meters = league *5556;
        return convert(meters, 10);
    }

    // foot - футы
    public double convertMToFoot() {
        double feet = meter *3.281;
        return convert(feet, 10);
    }

    public double convertFootToM() {
        double meters = foot /3.281;
        return convert(meters, 10);
    }


    public double convertMToYard() {
        double yards = meter *1.09361;
        return convert(yards, 10);
    }

    public double convertYardToM() {
        double meters = yard /1.09361;
        return convert(meters, 10);
    }
}


