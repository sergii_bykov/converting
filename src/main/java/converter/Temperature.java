package converter;

public class Temperature {

    public double C;
    public double kelvin_K;
    public double farenheit_F;
    public double reomur_Re;
    public double remer_Ro;
    public double rankin_Ra;
    public double newton_N;
    public double delil_D;

    public double convert(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    public double convertCToKelvin() {
        double kelvin = C+273.15;
        return convert(kelvin, 10);
    }

    public double convertKelvinToC() {
        double C = kelvin_K -273.15;
        return convert(C, 10);
    }

//    __________________________________________________

    public double convertCToFarenheit() {
        double farenheit = C*1.8+32;
        return convert(farenheit, 10);
    }

    public double convertFarenheitToC() {
        double C = (farenheit_F - 32)/1.8;
        return convert(C, 10);
    }

//  _____________________________________________________

    public double convertCToReomur() {
        double reomur = C*0.8;
        return convert(reomur, 10);
    }

    public double convertReomurToC() {

        double C = reomur_Re /0.8;
        return convert(C, 10);
    }

//  ______________________________________________________


    public double convertCToRemer() {
        double remer = C*0.525+7.5;
        return convert(remer, 10);
    }

    public double convertRemerToC() {
        double C = (remer_Ro - 7.5)/0.525;
        return convert(C, 10);
    }

//  _______________________________________________________

    public double convertCToRankin() {
        double rankin = C*1.8+491.47;
        return convert(rankin, 10);
    }

    public double convertRankinToC() {
        double C = (rankin_Ra - 491.47)/1.8;
        return convert(C, 10);
    }

//  _______________________________________________________

    public double convertCToNewton() {
        double newton = C*0.33;
        return convert(newton, 10);
    }

    public double convertNewtonToC() {
        double C = newton_N /0.33;
        return convert(C, 10);
    }

//  _______________________________________________________

    public double convertCToDelil() {
        double delil = (100 - C)*1.5;
        return convert(delil, 10);
    }

    public double convertDelilToC() {
        double C = -((delil_D /1.5)-100);
        return convert(C, 10);
    }

}

