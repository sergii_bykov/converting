package converter;

public class Volume {
    public double L;
    public double metreCubic;
    public double gallon;
    public double pint;
    public double quart;
    public double barrel;
    public double cubicFoot;
    public double cubicInch;

    public double convert(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }


    public double convertLToCubicMetre() {
        double cubicMetre = L/1000;
        return convert(cubicMetre, 10);
    }

    public double convertCubicMetreToL() {
        double L = metreCubic *1000;
        return convert(L, 10);
    }

//  _______________________________________________

    public double convertLToGallon() {
        double gallon = L/3.785;
        return convert(gallon, 10);
    }

    public double convertGallonToL() {
        double L = gallon*3.785;
        return convert(L, 10);
    }

//  ________________________________________________

    public double convertLToPint() {
        double pint = L*2.11338;
        return convert(pint, 10);
    }

    public double convertPintToL() {
        double L = pint/2.11338;
        return convert(L, 10);
    }

//  __________________________________________________

    public double convertLToQuart() {
        double quart = L*1.05669;
        return convert(quart, 10);
    }

    public double convertQuartToL() {
        double L = quart/1.05669;
        return convert(L, 10);
    }

//  __________________________________________________

    public double convertLToBarrel() {
        double barrel = L/159;
        return convert(barrel, 10);
    }

    public double convertBarrelToL() {
        double L = barrel*159;
        return convert(L, 10);
    }

//  __________________________________________________

    public double convertLToCubicFoot() {
        double cubicFoot = L/28.317;
        return convert(cubicFoot, 10);
    }

    public double convertCubicFootToL() {
        double L = cubicFoot*28.317;
        return convert(L, 10);
    }

//  __________________________________________________

    public double convertLToCubicInch() {
        double cubicInch = L*61.024;
        return convert(cubicInch, 10);
    }

    public double convertCubicInchToL() {
        double L = cubicInch/61.024;
        return convert(L, 10);
    }

}
