package services;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameClassService {
    int buttonsFrames[][] = new int[][]{
            {0, 0, 1, 1, 1, 25, 0, 0, 0, 0, 0},
            {0, 0, 1, 1, 1, 75, 0, 25, 0, 0, 0},
            {0, 0, 1, 1, 1, 125, 0, 25, 0, 0, 0},
            {0, 0, 1, 1, 1, 175, 0, 25, 0, 0, 0},
            {0, 0, 1, 1, 1, 225, 0, 25, 0, 0, 0},
            {0, 0, 1, 1, 1, 275, 0, 25, 0, 0, 0},
            {0, 0, 1, 1, 1, 325, 0, 25, 0, 0, 0}
    };

    String array[][][] = new String[][][]{
            {
                    {"Length", "m --> km", "km --> m", "convertMToKm", "convertKmToM"},
                    {"Length", "m --> mile", "mile --> m", "convertMToMile", "convertMileToM"},
                    {"Length", "m --> nautical mile", "nautical mile --> m", "convertMToNauticalMile", "convertNauticalMileToM"},
                    {"Length", "m --> cable", "cable --> m", "convertMToCable", "convertCableToM"},
                    {"Length", "m --> league", "league --> m", "convertMToLeague", "convertLeagueToM"},
                    {"Length", "m --> foot", "foot --> m", "convertMToFoot", "convertFootToM"},
                    {"Length", "m --> yard", "yard --> m", "convertMToYard", "convertYardToM"}
            },
            {
                    {"Time", "C --> K (Шкала Кельвина)", "K (Шкала Кельвина) --> C", "convertCToKelvin", "convertKelvinToC"},
                    {"Time", "C <--> F (Шкала Фаренгейта)", "F (Шкала Фаренгейта) --> C", "convertCToFarenheit", "convertFarenheitToC"},
                    {"Time", "C <--> Re (Шкала Реомюра)", "Re (Шкала Реомюра) --> C", "convertCToReomur", "convertReomurToC"},
                    {"Time", "C <--> Ro (Шкала Рёмера)", "Ro (Шкала Рёмера) --> C", "convertCToRemer", "convertRemerToC"},
                    {"Time", "C <--> Ra (Шкала Ранкина)", "Ra (Шкала Ранкина) --> C", "convertCToRankin", "convertRankinToC"},
                    {"Time", "C <--> N (Шкала Ньютона)", "N (Шкала Ньютона) --> C", "convertCToNewton", "convertNewtonToC"},
                    {"Time", "C <--> D (Шкала Делиля)", "D (Шкала Делиля) --> C", "convertCToDelil", "convertDelilToC"},

            },
            {
                    {"Temperature", "sec --> Min", "Min -->sec", "convertSecToMin", "convertMinToSec"},
                    {"Temperature", "sec --> Hour", "Hour -->sec", "convertSecToHour", "convertHourToSec"},
                    {"Temperature", "sec --> Day", "Day -->sec", "convertSecToDay", "convertDayToSec"},
                    {"Temperature", "sec --> Week", "Week -->sec", "convertSecToWeek", "convertWeekToSec"},
                    {"Temperature", "sec --> Month", "Month -->sec", "convertSecToMonth", "convertMonthToSec"},
                    {"Temperature", "sec --> Astronomical Year", "Astronomical Year -->sec", "convertSecToYear", "convertYearToSec"},
                    {"Temperature", "sec --> Astronomical Year", "Astronomical Year -->sec", "convertSecToYear", "convertYearToSec"},

            },
            {
                    {"Volume", "l --> m^3", "m^3 -->l", "convertLToCubicMetre", "convertCubicMetreToL"},
                    {"Volume", "l --> gallon", "gallon -->l", "convertLToGallon", "convertGallonToL"},
                    {"Volume", "l --> pint", "pint -->l", "convertLToPint", "convertPintToL"},
                    {"Volume", "l --> quart", "quart -->l", "convertLToQuart", "convertQuartToL"},
                    {"Volume", "l --> barrel", "barrel -->l", "convertLToBarrel", "convertBarrelToL"},
                    {"Volume", "l --> cubic foot", "cubic foot -->l", "convertLToCubicFoot", "convertCubicFootToL"},
                    {"Volume", "l <--> cubic inch", "cubic inch -->l", "convertLToCubicInch", "convertCubicInchToL"},


            },
            {
                    {"Weight", "kg --> g", "g -->kg", "convertKgToGram", "convertGramToKg"},
                    {"Weight", "kg --> carat", "carat -->kg", "convertKgToCarat", "convertCaratToKg"},
                    {"Weight", "kg --> eng pound", "eng pound -->kg", "convertKgToEngPound", "convertEngPoundToKg"},
                    {"Weight", "kg --> pound", "pound -->kg", "convertKgToPound", "convertPoundToKg"},
                    {"Weight", "kg --> pound", "pound -->kg", "convertKgToPound", "convertPoundToKg"},
                    {"Weight", "kg --> stone", "stone -->kg", "convertKgToStone", "convertStoneToKg"},
                    {"Weight", "kg --> rus funt", "rus funt -->kg", "convertKgToRusFunt", "convertRusFuntToKg"},


            }
    };

    public void action(String frameName, String lablel1Name, String lable2Name, String buttonConvert1Name, String buttonConvert2Name) {
        JFrame frame_1_1 = new JFrame(frameName);
        frame_1_1.setSize(500, 450);
        frame_1_1.setLocationRelativeTo(null);
        frame_1_1.setLayout(new GridBagLayout());

        JLabel label1_1 = new JLabel(lablel1Name);
        JTextField textField1_1 = new JTextField();
        JButton buttonConvert1 = new JButton("Convert");
        JLabel labelAnswer1 = new JLabel("Answer: ");

        JLabel label1_2 = new JLabel(lable2Name);
        JTextField textField1_2 = new JTextField();
        JButton buttonConvert2 = new JButton("Convert");
        JLabel labelAnswer2 = new JLabel("Answer: ");


        frame_1_1.add(label1_1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(25, 0, 0, 0), 0, 0));
        frame_1_1.add(textField1_1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(75, 0, 25, 0), 0, 0));
        frame_1_1.add(buttonConvert1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(125, 0, 25, 0), 0, 0));
        frame_1_1.add(labelAnswer1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(100, 0, 25, 0), 0, 0));

        frame_1_1.add(label1_2, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(175, 0, 0, 0), 0, 0));
        frame_1_1.add(textField1_2, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(225, 0, 25, 0), 0, 0));
        frame_1_1.add(buttonConvert2, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(275, 0, 25, 0), 0, 0));
        frame_1_1.add(labelAnswer2, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(250, 0, 25, 0), 0, 0));

        frame_1_1.setVisible(true);

        buttonConvert1.addActionListener(e -> {
            ButtonService buttonService = new ButtonService();
            String text = textField1_1.getText();
            labelAnswer1.setText(buttonService.launchButton(text, buttonConvert1Name));
        });

        buttonConvert2.addActionListener(e -> {
            ButtonService buttonService = new ButtonService();
            String text = textField1_2.getText();
            labelAnswer2.setText(buttonService.launchButton(text, buttonConvert2Name));

        });
    }

    public boolean IsDouble(String str) {
        boolean bool = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '0' || str.charAt(i) == '1' || str.charAt(i) == '2' || str.charAt(i) == '3'
                    || str.charAt(i) == '4' || str.charAt(i) == '5' || str.charAt(i) == '6' || str.charAt(i) == '7'
                    || str.charAt(i) == '8' || str.charAt(i) == '9' || (str.charAt(i) == '.' && str.length() > 1)) {
                bool = true;
            } else {
                bool = false;
                break;
            }
        }
        return bool;
    }

    public FrameClassService() {
        JFrame frame = new JFrame("Конвертер");
        frame.setSize(500, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        JLabel label1 = new JLabel("Выберите, что вы хотите преобразовать: ");


        JButton button1 = new JButton("Длина");
        JButton button2 = new JButton("Температура");
        JButton button3 = new JButton("Время");
        JButton button4 = new JButton("Объем");
        JButton button5 = new JButton("Масса");


        frame.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(25, 0, 0, 0), 0, 0));
        frame.add(button1, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(50, 0, 25, 0), 0, 0));
        frame.add(button2, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(100, 0, 25, 0), 0, 0));
        frame.add(button3, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(150, 0, 25, 0), 0, 0));
        frame.add(button4, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(200, 0, 25, 0), 0, 0));
        frame.add(button5, new GridBagConstraints(0, 0, 1, 1, 0.0, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(250, 0, 25, 0), 0, 0));

        frame.setVisible(true);

        button1.addActionListener(e -> {
            JFrame frame_1 = new JFrame("Length");
            frame_1.setSize(500, 450);
            frame_1.setLocationRelativeTo(null);
            frame_1.setLayout(new GridBagLayout());

            JButton[] buttonsArray = new JButton[]{
                    new JButton("m <--> km"),
                    new JButton("m <--> mile"),
                    new JButton("m <--> nautical mile"),
                    new JButton("m <--> cable"),
                    new JButton("m <--> league"),
                    new JButton("m <--> foot"),
                    new JButton("m <--> yard")
            };

            for (int i = 0; i < buttonsArray.length; i++) {
                frame_1.add(buttonsArray[i], new GridBagConstraints(
                        buttonsFrames[i][0],
                        buttonsFrames[i][1],
                        buttonsFrames[i][2],
                        buttonsFrames[i][3],
                        0.0,
                        buttonsFrames[i][4],
                        GridBagConstraints.NORTH,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(buttonsFrames[i][5],
                                buttonsFrames[i][6],
                                buttonsFrames[i][7],
                                buttonsFrames[i][8]),
                                buttonsFrames[i][9],
                                buttonsFrames[i][10]));

                int finalI = i;
                buttonsArray[i].addActionListener(e1 -> {
                    action(array[0][finalI][0], array[0][finalI][1], array[0][finalI][2], array[0][finalI][3], array[0][finalI][4]);
                });
            }

            frame_1.setVisible(true);
        });

        button2.addActionListener(e -> {
            JFrame frame_2 = new JFrame("Temperature");
            frame_2.setSize(500, 450);
            frame_2.setLocationRelativeTo(null);
            frame_2.setLayout(new GridBagLayout());

            JButton[] buttonsArray = new JButton[]{
            new JButton("C <--> K (Шкала Кельвина)"),
            new JButton("C <--> F (Шкала Фаренгейта)"),
            new JButton("C <--> Re (Шкала Реомюра)"),
            new JButton("C <--> Ro (Шкала Рёмера)"),
            new JButton("C <--> Ra (Шкала Ранкина)"),
            new JButton("C <--> N (Шкала Ньютона)"),
            new JButton("C <--> D (Шкала Делиля)"),

            };

            for (int i = 0; i < buttonsArray.length; i++) {
                frame_2.add(buttonsArray[i], new GridBagConstraints(
                        buttonsFrames[i][0],
                        buttonsFrames[i][1],
                        buttonsFrames[i][2],
                        buttonsFrames[i][3],
                        0.0,
                        buttonsFrames[i][4],
                        GridBagConstraints.NORTH,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(buttonsFrames[i][5],
                                buttonsFrames[i][6],
                                buttonsFrames[i][7],
                                buttonsFrames[i][8]),
                        buttonsFrames[i][9],
                        buttonsFrames[i][10]));

                int finalI = i;
                buttonsArray[i].addActionListener(e1 -> {
                    action(array[1][finalI][0], array[1][finalI][1], array[1][finalI][2], array[1][finalI][3], array[1][finalI][4]);
                });
            }

            frame_2.setVisible(true);
        });

        button3.addActionListener(e -> {
                JFrame frame_3 = new JFrame("Time");
                frame_3.setSize(500, 400);
                frame_3.setLocationRelativeTo(null);
                frame_3.setLayout(new GridBagLayout());

                JButton[] buttonsArray = new JButton[]{

                new JButton("sec <--> Min"),
                new JButton("sec <--> Hour"),
                new JButton("sec <--> Day"),
                new JButton("sec <--> Week"),
                new JButton("sec <--> Month"),
                new JButton("sec <--> Astronomical Year"),



                };

                for (int i = 0; i < buttonsArray.length; i++) {
                    frame_3.add(buttonsArray[i], new GridBagConstraints(
                            buttonsFrames[i][0],
                            buttonsFrames[i][1],
                            buttonsFrames[i][2],
                            buttonsFrames[i][3],
                            0.0,
                            buttonsFrames[i][4],
                            GridBagConstraints.NORTH,
                            GridBagConstraints.HORIZONTAL,
                            new Insets(buttonsFrames[i][5],
                                    buttonsFrames[i][6],
                                    buttonsFrames[i][7],
                                    buttonsFrames[i][8]),
                            buttonsFrames[i][9],
                            buttonsFrames[i][10]));

                    int finalI = i;
                    buttonsArray[i].addActionListener(e1 -> {
                        action(array[2][finalI][0], array[2][finalI][1], array[2][finalI][2], array[2][finalI][3], array[2][finalI][4]);
                    });
                }

                frame_3.setVisible(true);
        });

        button4.addActionListener(e -> {
                JFrame frame_4 = new JFrame("Volume");
                frame_4.setSize(500, 450);
                frame_4.setLocationRelativeTo(null);
                frame_4.setLayout(new GridBagLayout());

            JButton[] buttonsArray = new JButton[]{

                    new JButton("l <--> m^3"),
                new JButton("l <--> gallon"),
                new JButton("l <--> pint"),
                new JButton("l <--> quart"),
                new JButton("l <--> barrel"),
                new JButton("l <--> cubic foot"),
                new JButton("l <--> cubic inch")

            };
            for (int i = 0; i < buttonsArray.length; i++) {
                frame_4.add(buttonsArray[i], new GridBagConstraints(
                        buttonsFrames[i][0],
                        buttonsFrames[i][1],
                        buttonsFrames[i][2],
                        buttonsFrames[i][3],
                        0.0,
                        buttonsFrames[i][4],
                        GridBagConstraints.NORTH,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(buttonsFrames[i][5],
                                buttonsFrames[i][6],
                                buttonsFrames[i][7],
                                buttonsFrames[i][8]),
                        buttonsFrames[i][9],
                        buttonsFrames[i][10]));

                int finalI = i;
                buttonsArray[i].addActionListener(e1 -> {
                    action(array[3][finalI][0], array[3][finalI][1], array[3][finalI][2], array[3][finalI][3], array[3][finalI][4]);
                });
            }

            frame_4.setVisible(true);
        });

        button5.addActionListener(e -> {

            JFrame frame_5 = new JFrame("Weight");
                frame_5.setSize(500, 450);
                frame_5.setLocationRelativeTo(null);
                frame_5.setLayout(new GridBagLayout());
            JButton[] buttonsArray = new JButton[]{
                new JButton("kg <--> g"),
                    new JButton("kg <--> carat"),
                    new  JButton("kg <--> eng pound"),
                    new  JButton("kg <--> pound"),
                    new  JButton("kg <--> stone"),
                    new  JButton("kg <--> rus pound")

            };
            for (int i = 0; i < buttonsArray.length; i++) {
                frame_5.add(buttonsArray[i], new GridBagConstraints(
                        buttonsFrames[i][0],
                        buttonsFrames[i][1],
                        buttonsFrames[i][2],
                        buttonsFrames[i][3],
                        0.0,
                        buttonsFrames[i][4],
                        GridBagConstraints.NORTH,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(buttonsFrames[i][5],
                                buttonsFrames[i][6],
                                buttonsFrames[i][7],
                                buttonsFrames[i][8]),
                        buttonsFrames[i][9],
                        buttonsFrames[i][10]));

                int finalI = i;
                buttonsArray[i].addActionListener(e1 -> {
                    action(array[4][finalI][0], array[4][finalI][1], array[4][finalI][2], array[4][finalI][3], array[4][finalI][4]);
                });
            }

            frame_5.setVisible(true);
        });
                    }
                }



