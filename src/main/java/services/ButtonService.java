package services;

import converter.*;

public class ButtonService {
    private static final String MSG_SHOULD_DOUBLE_ORE_INT = "Here should be double or integer";
    private static final String MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE = "Here should be double or integer, cannot be a negative number";

    Services services = new Services();
    public String launchButton(String str, String method){
        String res = "";

//Length
        if (method.equalsIgnoreCase("convertMToKm")){
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToKm();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }
        else if(method.equalsIgnoreCase("convertKmToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.kilometers = Double.parseDouble(str);
                res = "Answer: "+ length.convertKmToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToMile")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToMile();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMileToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.mile = Double.parseDouble(str);
                res = "Answer: "+ length.convertMileToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToNauticalMile")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToNauticalMile();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertNauticalMileToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.nauticalMiles = Double.parseDouble(str);
                res = "Answer: "+ length.convertNauticalMileToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToCable")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToCable();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertCableToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.cable = Double.parseDouble(str);
                res = "Answer: "+ length.convertCableToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToLeague")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToLeague();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLeagueToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.league = Double.parseDouble(str);
                res = "Answer: "+ length.convertLeagueToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToFoot")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToFoot();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertFootToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.foot = Double.parseDouble(str);
                res = "Answer: "+ length.convertFootToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMToYard")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.meter = Double.parseDouble(str);
                res = "Answer: "+ length.convertMToYard();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertYardToM")) {
            Length length = new Length();
            if(services.IsDouble(str)){
                length.yard = Double.parseDouble(str);
                res = "Answer: "+ length.convertYardToM();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        //Temperature

        else if(method.equalsIgnoreCase("convertCToKelvin")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToKelvin();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertKelvinToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.kelvin_K = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertKelvinToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToFarenheit")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToFarenheit();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertFarenheitToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.farenheit_F = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertFarenheitToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToReomur")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToReomur();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertReomurToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.reomur_Re = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertReomurToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToRemer")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToRemer();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertRemerToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.remer_Ro = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertRemerToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToRankin")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToRankin();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertRankinToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.rankin_Ra = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertRankinToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToNewton")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToNewton();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertNewtonToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.newton_N = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertNewtonToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertCToDelil")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.C = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertCToDelil();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        else if(method.equalsIgnoreCase("convertDelilToC")) {
            Temperature temperature = new Temperature();
            if(services.IsDouble(str)){
                temperature.delil_D = Double.parseDouble(str);
                res = "Answer: "+ temperature.convertDelilToC();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT;
            }
        }

        //Time

        else if(method.equalsIgnoreCase("convertSecToMin")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToMin();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMinToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.min = Double.parseDouble(str);
                res = "Answer: "+ time.convertMinToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertSecToHour")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToHour();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertHourToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.hour = Double.parseDouble(str);
                res = "Answer: "+ time.convertHourToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertSecToDay")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToDay();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertDayToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.day = Double.parseDouble(str);
                res = "Answer: "+ time.convertDayToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertSecToWeek")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToWeek();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertWeekToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.week = Double.parseDouble(str);
                res = "Answer: "+ time.convertWeekToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertSecToMonth")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToMonth();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertMonthToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.month = Double.parseDouble(str);
                res = "Answer: "+ time.convertMonthToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertSecToYear")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.sec = Double.parseDouble(str);
                res = "Answer: "+ time.convertSecToYear();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertYearToSec")) {
            Time time = new Time();
            if(services.IsDouble(str)){
                time.astronomicalYear = Double.parseDouble(str);
                res = "Answer: "+ time.convertYearToSec();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        //Volume

        else if(method.equalsIgnoreCase("convertLToCubicMetre")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToCubicMetre();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertCubicMetreToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.metreCubic = Double.parseDouble(str);
                res = "Answer: "+ volume.convertCubicMetreToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToGallon")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToGallon();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertGallonToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.gallon = Double.parseDouble(str);
                res = "Answer: "+ volume.convertGallonToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToPint")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToPint();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertPintToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.pint = Double.parseDouble(str);
                res = "Answer: "+ volume.convertPintToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToQuart")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToQuart();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertQuartToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.quart = Double.parseDouble(str);
                res = "Answer: "+ volume.convertQuartToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToBarrel")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToBarrel();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertBarrelToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.barrel = Double.parseDouble(str);
                res = "Answer: "+ volume.convertBarrelToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToCubicFoot")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToCubicFoot();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertCubicFootToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.cubicFoot = Double.parseDouble(str);
                res = "Answer: "+ volume.convertCubicFootToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertLToCubicInch")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.L = Double.parseDouble(str);
                res = "Answer: "+ volume.convertLToCubicInch();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertCubicInchToL")) {
            Volume volume = new Volume();
            if(services.IsDouble(str)){
                volume.cubicInch = Double.parseDouble(str);
                res = "Answer: "+ volume.convertCubicInchToL();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        //Weight

        else if(method.equalsIgnoreCase("convertKgToGram")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToGram();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertGramToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.gram = Double.parseDouble(str);
                res = "Answer: "+ weight.convertGramToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertKgToCarat")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToCarat();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertCaratToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.carat = Double.parseDouble(str);
                res = "Answer: "+ weight.convertCaratToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertKgToEngPound")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToEngPound();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertEngPoundToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.engPound = Double.parseDouble(str);
                res = "Answer: "+ weight.convertEngPoundToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertKgToPound")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToPound();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertPoundToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.pound = Double.parseDouble(str);
                res = "Answer: "+ weight.convertPoundToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertKgToStone")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToStone();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertStoneToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.stone = Double.parseDouble(str);
                res = "Answer: "+ weight.convertStoneToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertKgToRusFunt")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.kg = Double.parseDouble(str);
                res = "Answer: "+ weight.convertKgToRusPound();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        else if(method.equalsIgnoreCase("convertRusFuntToKg")) {
            Weight weight = new Weight();
            if(services.IsDouble(str)){
                weight.rusPound = Double.parseDouble(str);
                res = "Answer: "+ weight.convertRusPoundToKg();
            }
            else if(!services.IsDouble(str)){
                res = MSG_SHOULD_DOUBLE_ORE_INT_CANNOT_NEGATIVE;
            }
        }

        return res;
    }
}


